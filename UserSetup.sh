#!/usr/bin/env sh

################################################################################
# Setup                                                                        #
# Copyright (C) 2023-2025 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Exit if not in terminal
if [ ! -t 0 ]; then
    exit 1
fi

# Solve weird wayland bug and remove some more theming
rm ~/.config/kdeglobals

# Global theme
lookandfeeltool -a 'org.kde.breezedark.desktop'

# Desktop wallpaper
plasma-apply-wallpaperimage ./Wallpaper.png

touch ~/.bash_profile

# Disable GTK CSD
if [ $(grep -c -Pz 'export GTK_CSD=0' ~/.bash_profile) -eq 0 ]; then
    echo $'\nexport GTK_CSD=0' >> ~/.bash_profile
fi
if [ $(grep -c -Pz 'export LD_PRELOAD=libgtk3-nocsd.so.0' ~/.bash_profile) -eq 0 ]; then
    echo $'\nexport LD_PRELOAD=libgtk3-nocsd.so.0' >> ~/.bash_profile
fi

# Enable portal
if [ $(grep -c -Pz 'export GTK_USE_PORTAL=1' ~/.bash_profile) -eq 0 ]; then
    echo $'\nexport GTK_USE_PORTAL=1' >> ~/.bash_profile
fi

# Download and set profile picture
curl https://codeberg.org/avatars/1ce30314699ee26921a6d64bc735815e \
    --output ~/Pictures/Profile.png
busctl call org.freedesktop.Accounts /org/freedesktop/Accounts/User`id -u` \
    org.freedesktop.Accounts.User SetIconFile s ~/Pictures/Profile.png

# Set up git signature, editor and commit mailing
git config --global user.email "morsmortium@disroot.org"
git config --global user.name "MorsMortium"
git config --global core.editor "kate -b"
git config --global sendemail.smtpserver "disroot.org"
git config --global sendemail.smtpuser "morsmortium@disroot.org"
git config --global sendemail.smtpencryption "tls"
git config --global sendemail.smtpserverport "587"

# Clone repos
mkdir ~/Projects
cd ~/Projects
git clone https://codeberg.org/MorsMortium/Configurations
git clone https://codeberg.org/MorsMortium/Secrets
git clone https://codeberg.org/MorsMortium/LBRY-GTK
git clone https://codeberg.org/MorsMortium/Script
cd ~

read -p "Is Latte Dock fixed already for Plasma 6? [yn]" answer
if [[ $answer = y ]] ; then
    # Delete KDE panel, set up Latte dock
    qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript \
        'panelById(panelIds[0]).remove();'
    if [ $(latte-dock --available-layouts 2> >(grep -c -Pz '"MorsMortium"')) -eq 0 ]; then
        latte-dock --import-layout ~/Projects/Configurations/MorsMortium.layout.latte &
    fi
    latte-dock --layout MorsMortium --replace &
fi

# Set up notes
touch ~/Documents/Notes
rm ~/.local/share/plasma_notes/b2cd63e4-6139-4187-aeba-418af9a39c
mkdir -p ~/.local/share/plasma_notes
ln ~/Documents/Notes ~/.local/share/plasma_notes/b2cd63e4-6139-4187-aeba-418af9a39c

# Set up autostart
rm ~/.config/autostart/*
mkdir -p ~/.config/autostart
cd ~/.config/autostart
cp /usr/share/applications/emote.desktop ./
cp /usr/share/applications/org.kde.latte-dock.desktop ./
cp /usr/share/applications/org.kde.yakuake.desktop ./
cp /usr/share/applications/org.kde.kmail2.desktop ./
cp /usr/share/applications/nheko.desktop ./
cp /usr/share/applications/org.gajim.Gajim.desktop ./
cp /usr/share/applications/de.manic.Friendiqa.desktop ./
cp /usr/share/applications/org.qbittorrent.qBittorrent.desktop ./
echo $'[Desktop Entry]\nExec=/home/morsmortium/Projects/Script/KeePassXC.sh\nIcon=dialog-scripts\nName=KeePassXC.sh\nPath=\nType=Application\nX-KDE-AutostartScript=true\n' > ./KeePassXC.sh.desktop
echo $'[Desktop Entry]\nExec=pipewire & pipewire-pulse & wireplumber\nIcon=\nName=\nPath=\nTerminal=False\nType=Application\n' > ./pipewire.desktop
echo $'[Desktop Entry]\nExec=harbour-amazfishd\nIcon=\nName=\nPath=\nTerminal=False\nType=Application\n' > ./harbour-amazfishd.desktop
echo $'[Desktop Entry]\nExec=syncthing -no-browser\nIcon=\nName=\nPath=\nTerminal=False\nType=Application\n' > ./syncthing.desktop
cd ~

# Create Waydroid Stop icon
mkdir -p ~/.local/share/icons/hicolor/512x512/apps/
magick /usr/share/icons/hicolor/512x512/apps/waydroid.png -fill red -stroke red -strokewidth 50 -draw "stroke-linecap round line 25,25 487,487" ~/.local/share/icons/hicolor/512x512/apps/waydroid-stop.png

# Create Waydroid Stop application
mkdir -p ~/.local/share/applications
echo $'[Desktop Entry]\nExec=waydroid session stop\nIcon=waydroid-stop\nName=Waydroid Stop\nType=Application\nCategories=X-WayDroid-App\n' > ~/.local/share/applications/waydroid-stop.desktop

# Restore double click to open
kwriteconfig6 --file ~/.config/kdeglobals --group KDE --key SingleClick "false"

# Make Dolphin remember each folders view
kwriteconfig6 --file ~/.config/dolphinrc --group General --key GlobalViewProps "false"

# Set KeePassXC password and phone pin code
read -s -p "KeePassXC password: " password
kwalletcli  -f 'Passwords' -e 'KeePassXC' -p "$password"
unset password
read -s -p "Pin code: " password
kwalletcli  -f 'Passwords' -e 'PIN' -p "$password"
unset password

# Remove every virtual desktop
oldremove=""
while : ; do
    oldremove=$remove
    remove=$(qdbus --literal org.kde.KWin /VirtualDesktopManager desktops | grep "[0-9a-z\-]\{36\}" -o | tail -1)
    qdbus org.kde.KWin /VirtualDesktopManager removeDesktop $remove
    [[ $oldremove != $remove ]] || break
done

# Add 4 more virtual desktops
for i in {0..3} ; do
    qdbus org.kde.KWin /VirtualDesktopManager createDesktop 5 ""
done

# Start with empty session
kwriteconfig6 --file ~/.config/ksmserverrc --group General --key loginMode \
    "emptySession"

# Set favorites
sqlite3 ~/.local/share/kactivitymanagerd/resources/database << MultiLine
    DELETE FROM ResourceLink WHERE initiatingAgent='org.kde.plasma.favorites.applications';
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:org.kde.ksysguard.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:systemsettings.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:org.kde.kate.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:audacious.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:org.gnome.Glade.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:media-downloader.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:com.github.marinm.songrec.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:pysol.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:manaplus.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:org.gnome.TwentyFortyEight.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:net.lutris.Lutris.desktop');
    INSERT INTO ResourceLink VALUES (':global', 'org.kde.plasma.favorites.applications', 'applications:org.kde.discover.desktop');
MultiLine

# Set default applications
sed -i '/^video\// d' ~/.config/mimeapps.list
sed -i '/^audio\// d' ~/.config/mimeapps.list
sed -i '/^image\// d' ~/.config/mimeapps.list
xdg-mime default org.kde.kate.desktop text/plain
xdg-mime default org.kde.gwenview.desktop `grep ^Mime /usr/share/applications/org.kde.gwenview.desktop | cut -c 10- | tr ';' ' '`
xdg-mime default org.kde.haruna.desktop `grep ^Mime /usr/share/applications/org.kde.haruna.desktop | cut -c 10- | tr ';' ' '`
xdg-mime default audacious.desktop `grep ^Mime /usr/share/applications/audacious.desktop | cut -c 10- | tr ';' ' '`
xdg-mime default org.inkscape.Inkscape.desktop `grep ^Mime /usr/share/applications/org.inkscape.Inkscape.desktop | cut -c 10- | tr ';' ' '`
xdg-mime default okularApplication_pdf.desktop `grep ^Mime /usr/share/applications/okularApplication_pdf.desktop | cut -c 10- | tr ';' ' '`
xdg-mime default org.kde.dolphin.desktop `grep ^Mime /usr/share/applications/org.kde.dolphin.desktop | cut -c 10- | tr ';' ' '`
xdg-mime default audacious.desktop x-content/audio-player

# Set unlimited bash history
touch ~/.bashrc
if [ $(grep -c -Pz 'export HISTFILESIZE=' ~/.bashrc) -eq 0 ]; then
    echo $'\nexport HISTFILESIZE=' >> ~/.bashrc
fi

if [ $(grep -c -Pz 'export HISTSIZE=' ~/.bashrc) -eq 0 ]; then
    echo $'\nexport HISTSIZE=' >> ~/.bashrc
fi

# Enable Kirigami low power mode
if [ $(grep -c -Pz 'export KIRIGAMI_LOWPOWER_HARDWARE=1' ~/.bash_profile) -eq 0 ]; then
    echo $'\nexport KIRIGAMI_LOWPOWER_HARDWARE=1' >> ~/.bash_profile
fi

# Relocate go
if [ $(grep -c -Pz 'export GOPATH=$HOME/.local/share/go' ~/.bash_profile) -eq 0 ]; then
    echo $'\nexport GOPATH=$HOME/.local/share/go' >> ~/.bash_profile
fi
mv ~/go ~/.local/share/go

# Relocate rust
if [ $(grep -c -Pz 'export CARGO_HOME=$HOME/.local/share/cargo' ~/.bash_profile) -eq 0 ]; then
    echo $'\nexport CARGO_HOME=$HOME/.local/share/cargo' >> ~/.bash_profile
fi
mv ~/.cargo ~/.local/share/cargo

# Set up aliases

if [ $(grep -c -Pz 'alias History=~/Projects/Script/History.sh' ~/.bashrc) -eq 0 ]; then
    echo $'\nalias History=~/Projects/Script/History.sh' >> ~/.bashrc
fi

if [ $(grep -c -Pz 'alias AutoRemove=~/Projects/Script/AutoRemove.sh' ~/.bashrc) -eq 0 ]; then
    echo $'\nalias AutoRemove=~/Projects/Script/AutoRemove.sh' >> ~/.bashrc
fi

if [ $(grep -c -Pz 'alias Upgrade=~/Projects/Script/Upgrade.sh' ~/.bashrc) -eq 0 ]; then
    echo $'\nalias Upgrade=~/Projects/Script/Upgrade.sh' >> ~/.bashrc
fi

if [ $(grep -c -Pz 'alias HistoryTrim=~/Projects/Script/HistoryTrim.sh' ~/.bashrc) -eq 0 ]; then
    echo $'\nalias HistoryTrim=~/Projects/Script/HistoryTrim.sh' >> ~/.bashrc
fi
