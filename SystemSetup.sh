#!/usr/bin/env sh

################################################################################
# Setup                                                                        #
# Copyright (C) 2023-2025 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Exit if not in terminal
if [ ! -t 0 ]; then
    exit 1
fi

# Remove theming and other unneded packages
sudo pacman -D --asdeps artix-desktop-presets artix-branding-base nvidia-utils \
    pulseaudio-zeroconf artix-backgrounds artix-gtk-presets artix-icons nvidia \
    artix-qt-presets sddm-theme-artix artix-grub-theme artix-dark-theme falkon \
    konqueror kcalc noto-fonts-emoji
sudo pacman -Rns $(pacman -Qqtd)

# Remove hardcoded theming
sudo sed -i /QT_STYLE_OVERRIDE=gtk2/d  /etc/environment
sudo sed -i /QT_QPA_PLATFORMTHEME=gtk/d  /etc/environment

# Upgrade
sudo pacman -Syyu --needed

# Enable Arch repo support
sudo pacman -S --needed artix-archlinux-support

# Add Arch mirrors
if [ $(grep -c -Pz '\n\[extra\]\nInclude = /etc/pacman.d/mirrorlist-arch' /etc/pacman.conf) -eq 0 ]; then
    echo $'\n[extra]\nInclude = /etc/pacman.d/mirrorlist-arch'  | sudo tee -a /etc/pacman.conf
fi

# Enable 32bit packages
sudo sed -zi 's;#\[lib32\]\n#Include = /etc/pacman.d/mirrorlist;\[lib32\]\nInclude = /etc/pacman.d/mirrorlist;g' /etc/pacman.conf

# Enable debug packages
if [ $(grep -c -Pz '\n\[core-debug\]\nServer = https://geo.mirror.pkgbuild.com/\$repo/os/\$arch' /etc/pacman.conf) -eq 0 ]; then
    echo $'\n[core-debug]\nServer = https://geo.mirror.pkgbuild.com/$repo/os/$arch'  | sudo tee -a /etc/pacman.conf
fi

if [ $(grep -c -Pz '\n\[extra-debug\]\nServer = https://geo.mirror.pkgbuild.com/\$repo/os/\$arch' /etc/pacman.conf) -eq 0 ]; then
    echo $'\n[extra-debug]\nServer = https://geo.mirror.pkgbuild.com/$repo/os/$arch'  | sudo tee -a /etc/pacman.conf
fi

if [ $(grep -c -Pz '\n\[multilib-debug\]\nServer = https://geo.mirror.pkgbuild.com/\$repo/os/\$arch' /etc/pacman.conf) -eq 0 ]; then
    echo $'\n[multilib-debug]\nServer = https://geo.mirror.pkgbuild.com/$repo/os/$arch'  | sudo tee -a /etc/pacman.conf
fi

# Add core repo to install glibc for valgrind
if [ $(grep -c -Pz '\n\[core\]\nServer = https://geo.mirror.pkgbuild.com/\$repo/os/\$arch' /etc/pacman.conf) -eq 0 ]; then
    echo $'\n[core]\nServer = https://geo.mirror.pkgbuild.com/$repo/os/$arch'  | sudo tee -a /etc/pacman.conf
fi

# Remove existing glibc package
sudo rm /var/cache/pacman/pkg/glibc*

# Refresh mirrors, upgrade
sudo pacman -Syyu

# Install needed packages
# Reasons (where not self evident):
# unrar, unarchiver: ark rar support
# go: dependency of yay
# xclip, wl-clipboard: copy for keepassxc
# core/glibc: valgrind support
# sof-firmware: ploopy headphones
# anything lib32: wine
# vulkan-intel: wine vulkan support
# qt6-imageformats: webp support in gwenview
# qt6-languageserver: QML LSP support
# freeimage: unset dependency of freej2me-git
# libappindicator-gtk3: system tray icon for Gajim
# kimageformats: extends supported image formats in KDE and Qt apps
# fwupd: firmware info in Info Center
# sysbench, iperf3: benchmarks for hardinfo
sudo pacman -S --needed unrar keepassxc audacious firefox ydotool qalculate-qt \
    pysolfc bleachbit krita chromium filelight fontforge gajim freecad puzzles \
    glade gnome-pie neovim gwenview haruna inkscape kdeconnect kdenlive kdiff3 \
    qbittorrent qtqr skanlite spectacle nheko gnome-tetravex yakuake qtcreator \
    kde-system-meta merkuro blender git base-devel go man valgrind cmake godot \
    python-pip kcolorchooser kwalletmanager usbutils xclip songrec bluez-utils \
    wl-clipboard kate xdg-desktop-portal-gtk arj qt5-wayland massif-visualizer \
    prusa-slicer kde-games-meta dbus-broker obs-studio tenacity freeimage btop \
    virt-manager dnsmasq qemu-desktop gamemode zip wine-staging packagekit-qt6 \
    plymouth-kcm plasma-meta plasma-wayland-protocols torbrowser-launcher vmpk \
    breeze-plymouth libktorrent kcharselect winetricks lutris mangohud labplot \
    libvirt-openrc picard maliit-keyboard core/glibc pipewire-alsa wireplumber \
    pipewire-pulse pipewire-jack sof-firmware iio-sensor-proxy dolphin-plugins \
    retroarch-assets-ozone lib32-alsa-lib lib32-alsa-plugins vulkan-icd-loader \
    lib32-vulkan-icd-loader lib32-vulkan-intel kmail ark okteta flatpak kclock \
    lib32-libpulse lib32-pipewire kiwix-desktop vulkan-intel lib32-mesa namcap \
    plasma-sdk uncrustify fastfetch qt6-imageformats qt6-languageserver yt-dlp \
    kalarm python-pipx unarchiver krfb kmag krdc qtractor libappindicator-gtk3 \
    libreoffice-fresh nextcloud-client kolourpaint okular kruler luanti fdupes \
    power-profiles-daemon-openrc rebuild-detector noto-fonts-cjk linux-headers \
    kimageformats fwupd kfind sysbench iperf3

# Enable virtualization service
sudo rc-update add libvirtd
sudo rc-service libvirtd start

# Enable power profile manager service
sudo rc-update add power-profiles-daemon
sudo rc-service power-profiles-daemon start

# Set Virtual Machine Manager network to start automatically
sudo virsh net-autostart default

# Set up time sync
sudo rc-update add ntpd
sudo rc-service ntpd start

# Build and install yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg
sudo pacman -U $(ls ./*.pkg.tar.zst| head -1)

# Install needed AUR packages
# Reasons (where not self evident):
# gtkttk, ttk-theme-chooser: theming TTK apps with GTK2 theme
# waydroid-script-git: add ARM support
yay -S --needed libretro-pcsx-rearmed-git pysolfc-music grub2-theme-breeze-git \
    gnome-games-meta appimagelauncher media-downloader gtkttk webcamoid ventoy \
    latte-dock-git powdertoy-bin otter-browser syncthing-gtk friendiqa trackma \
    spacecadetpinball-git blender-breezedark-theme-git ksysguard6-git waydroid \
    git-cola plasma-settings pygubu-designer manaplus-sdl2 netbird 0x0uploader \
    ttf-noto-emoji-monochrome luckey-games-meta waydroid-script-git signal-cli \
    bluetooth-autoconnect ttk-theme-chooser freej2me-git kwalletcli antimicrox \
    harbour-amazfish gtk3-nocsd-git downgrade hardinfo sdiff-gtk alpaka-git \
    emote
    # k3d-git eka2l1

read -p "Set GRUB and splash theme (breaks Gole 2 Pro)? [yn]" answer
if [[ $answer = y ]] ; then
    # Set GRUB theme
    sudo sed -i 's;^GRUB_THEME=".*;GRUB_THEME="/usr/share/grub/themes/breeze/theme.txt";g' /etc/default/grub
    sudo grub-mkconfig -o /boot/grub/grub.cfg

    # Set splash screen
    echo 'HOOKS=$HOOKS" plymouth"'  | sudo tee /etc/mkinitcpio.conf.d/plymouth.conf
    sudo mkinitcpio -P
    if [ $(grep -c -Pz 'GRUB_CMDLINE_LINUX_DEFAULT=\$GRUB_CMDLINE_LINUX_DEFAULT" splash rd.udev.log_priority=3 vt.global_cursor_default=0"' /etc/default/grub) -eq 0 ]; then
        echo $'\nGRUB_CMDLINE_LINUX_DEFAULT=$GRUB_CMDLINE_LINUX_DEFAULT" splash rd.udev.log_priority=3 vt.global_cursor_default=0"'  | sudo tee -a /etc/default/grub
    fi
    sudo grub-mkconfig -o /boot/grub/grub.cfg
    sudo plymouth-set-default-theme -R breeze
fi

# Ploopy Toolbox fix
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="fedd", MODE="666"' | sudo tee /etc/udev/rules.d/100-ploopy-headphones.rules
sudo udevadm control --reload-rules && sudo udevadm trigger

# Set maliit keyboard theme
if [ $(grep -c -Pz 'QT_QUICK_CONTROLS_STYLE=org.kde.desktop' /etc/environment) -eq 0 ]; then
    echo $'\nQT_QUICK_CONTROLS_STYLE=org.kde.desktop'  | sudo tee -a /etc/environment
fi

# Limit fix for Lutris
if [ $(grep -c -Pz 'morsmortium hard nofile 524288' /etc/security/limits.conf) -eq 0 ]; then
    echo $'\nmorsmortium hard nofile 524288'  | sudo tee -a /etc/security/limits.conf
fi

# Phone specific fixes

read -p "Rotate touch, gyroscope input and disable phantom devices for the Gole 2 Pro? [yn]" answer
if [[ $answer = y ]] ; then
    # Rotate touch input by 180 degrees, rotate gyroscope sensor, disable phantom devices
    echo $'ENV{LIBINPUT_CALIBRATION_MATRIX}="-1 0 1 0 -1 1"\nENV{ACCEL_MOUNT_MATRIX}="0, -1, 0; -1, 0, 0; 0, 0, 1"\nACTION!="remove",KERNEL=="event[0-9]*",ATTRS{name}=="Intel HID 5 button array",ENV{LIBINPUT_IGNORE_DEVICE}="1"\nACTION!="remove",KERNELS=="input2",ATTRS{name}=="Power Button",ENV{LIBINPUT_IGNORE_DEVICE}="1"' | sudo tee \
        /etc/udev/rules.d/99-gole2pro.rules
fi

# Set up screen rotation
echo $'#!/sbin/openrc-run\ncommand=/usr/lib/iio-sensor-proxy\ncommand_background=yes\npidfile=/run/iio-sensor-proxy.pid\ndepend() {\n\tneed dbus localmount\n}' | sudo tee /etc/init.d/iio-sensor-proxy
sudo chmod +x /etc/init.d/iio-sensor-proxy
sudo rc-update add iio-sensor-proxy default
sudo rc-service iio-sensor-proxy start

read -p "Fix missing speaker audio for the Gole 2 Pro? [yn]" answer
if [[ $answer = y ]] ; then
    # Fix missing speaker audio on muted headphones
    sudo kwriteconfig6 --file \
        /usr/share/alsa-card-profile/mixer/paths/analog-output-speaker.conf \
        --group "Element Headphone" --key switch mute
    sudo kwriteconfig6 --file \
        /usr/share/alsa-card-profile/mixer/paths/analog-output-speaker.conf \
        --group "Element Headphone" --key volume merge
fi
