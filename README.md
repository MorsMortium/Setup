# Setup

My desktop setup, currently on Artix Linux

# Issues

There are a number of bugs, misfeatures and such, that occur semi daily, when using this system. Here is a detailed list: [Issues.md](Issues.md). Finding or making bug reports and listing them, as well as updating the list is in progress.

# TODO

## Research needed

- Set GTK theme (for all GTK versions, as System Settings does)
- Set SDDM theme (with font, cursor, etc., as System Settings does)
- Install Pling items (plasma widgets)
    - https://www.pling.com/p/1506284
    - https://www.pling.com/p/1584342
- Set proper favorite order
- Manjaro Settings Manager

## (Possibly) Upstream work needed

- AUR packages
    - K3D-git
    - Hexeditor

## Miscellaneous

- Format the script to keep 80 characters length
- Make as unmanned as possible
- Any small additions that comes up later

For possible Pling solution: https://aur.archlinux.org/packages/ocs-url
