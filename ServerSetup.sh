#!/usr/bin/env sh

################################################################################
# Setup                                                                        #
# Copyright (C) 2023-2025 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Exit if not in terminal
if [ ! -t 0 ]; then
    exit 1
fi

# Remove theming and other unneded packages
sudo pacman -D --asdeps artix-desktop-presets artix-branding-base nvidia-utils \
    pulseaudio-zeroconf artix-backgrounds artix-gtk-presets artix-icons nvidia \
    artix-qt-presets sddm-theme-artix artix-grub-theme artix-dark-theme falkon \
    konqueror kcalc noto-fonts-emoji
sudo pacman -Rns $(pacman -Qqtd)

# Upgrade
sudo pacman -Syu --needed

# Enable Arch repo support
sudo pacman -S --needed artix-archlinux-support

# Add Arch mirrors
if [ $(grep -c -Pz '\n\[extra\]\nInclude = /etc/pacman.d/mirrorlist-arch' /etc/pacman.conf) -eq 0 ]; then
    echo $'\n[extra]\nInclude = /etc/pacman.d/mirrorlist-arch'  | sudo tee -a /etc/pacman.conf
fi

# Refresh mirrors, upgrade
sudo pacman -Syu

# Install needed packages
# Reasons (where not self evident):
# go: dependency of yay
# prosody python-pipx: multi-protocol messaging client setup
sudo pacman -S --needed home-assistant python-pipx base-devel fastfetch x11vnc \
    git go neovim prosody btop firefox docker kdeconnect

# Set up time sync
sudo rc-update add ntpd
sudo rc-service ntpd start

# Build and install yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg
sudo pacman -U $(ls ./*.pkg.tar.zst| head -1)

# Install needed AUR packages
yay -S --needed jackett biboumi-git downgrade netbird webcamoid

# Enable and start netbird service
sudo netbird service install 
sudo netbird service start

# Install needed docker packages
sudo dockerd
sudo docker pull nextcloud homeassistant/home-assistant searxng/searxng

