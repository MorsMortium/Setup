#!/usr/bin/env sh

################################################################################
# Setup                                                                        #
# Copyright (C) 2023-2025 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Start all services
sudo dockerd &
sudo docker run -d -p 8080:80 -v nextcloud:/var/www/html -v data:/var/www/html/data -v config:/var/www/html/config nextcloud
sudo docker run -d -v haconfig:/config --net=host --device=/dev/ttyUSB0 -v /var/run/dbus:/var/run/dbus homeassistant/home-assistant
sudo docker run -d -p 8888:8080 searxng/searxng

# sudo docker exec --user www-data -it [containername] php occ config:system:set trusted_domains 10 --value="[ip]"

# Keep VNC open
while true ; do x11vnc -reopen -forever ; done
